# ABOUT

This Git repository contains the source files for https://www.davidnind.com, the personal website of David Nind, Wellington, New Zealand.

# TOPICS COVERED

The site includes a blog and covers these topics:

* Free and open source software.
* Projects I am interested in, such as [Drupal] for content management, [Koha] for library management, and [Piwik] for website statistics.
* DITA XML, an XML architecture and OASIS standard for designing, writing, managing, and publishing topic-based, modular, and reusable documentation in multiple formats.

# TECHNOLOGY USED

* [Git] for version control
* [GitLab] for Git repository management
* [GitLab Pages] for site hosting
* [Jekyll] static site generator
* [Let's Encrypt] for free certficates

# LICENCE FOR CONTENT

![alt text][cc-by-4-image]

&copy; David Nind 2016

The content on this site is licensed under a [Creative Commons Attribution 4.0 International] licence unless otherwise noted.

[Creative Commons Attribution 4.0 International]: https://creativecommons.org/licenses/by/4.0/
[Drupal]: https://www.drupal.org/
[Git]: https://git-scm.com/
[GitLab]: https://gitlab.com/
[GitLab Pages]: https://pages.gitlab.io/
[Jekyll]: https://jekyllrb.com/
[Koha]: https://koha-community.org
[Let's Encrypt]: https://letsencrypt.org/
[Piwik]: https://piwik.org/

[cc-by-4-image]: https://i.creativecommons.org/l/by/4.0/88x31.png "Creative Commons Licence"
