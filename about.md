---
layout: page
title: About
permalink: /about/
---

# A bit about me

<img style="float:right; margin: auto;" src="/images/photo-david-nind.jpg" width="125" alt="Photo of David Nind"/>I live in [Oamaru](https://en.wikipedia.org/wiki/Oamaru), [New Zealand](https://en.wikipedia.org/wiki/New_Zealand).

I currently volunteer for the [Koha Community (library management software)](https://koha-community.org) and [Wikipedia](https://en.wikipedia.org/wiki/User:David_Nind) (Wikidata, Commons, and Wikisource).

I worked at Inland Revenue in Wellington until March 2022, where I was part of the support team for the Policy and Regulatory Stewardship group. My former colleagues continue to do [interesting things](https://taxpolicy.ird.govt.nz) (if you like tax, and tax policy). Things that kept me busy included web content management and information management, among other things.

# Contacting me

* Email: david AT davidnind DOT com
* Mastodon: [@davidnind@mastodon.nzoss.nz](https://mastodon.nzoss.nz/@davidnind)
* Twitter: [@DavidNind](https://twitter.com/DavidNind)
* Phone: +64 21 0537 847

# Tools used to create this site

* [Git] for version control
* [GitLab] for Git repository management
* [GitLab Pages] for site hosting
* [Jekyll] static site generator
* [Let's Encrypt] for free security certificates

You can find the source code for this site and its content at [GitLab](https://gitlab.com/davidnind/davidnind.gitlab.io/).

[Git]: https://git-scm.com/
[GitLab]: https://gitlab.com/
[GitLab Pages]: https://about.gitlab.com/stages-devops-lifecycle/pages/
[Jekyll]: https://jekyllrb.com/
[Let's Encrypt]: https://letsencrypt.org/
