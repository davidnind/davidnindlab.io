---
layout: post
title:  "Wikimedia Commons Tip - Quickly see image usage"
date:   2023-02-14 08:40:00 +1300
---

The Global Usage Badges gadget lets you quickly see how many times images listed on a media category page are used.

This makes it much easier to see image usage, instead of looking at individual image pages.

An example of the Global Usage Badges gadget in use:

![An example of the Global Usage Badges gadget in use - each preview image has a small box in the bottom right-hand corner showing the number of times it is used](/images/2023-02-14-wikimedia-commons-global-usage-badges-in-use.png)

To enable the gadget:
1. Go to [Wikimedia Commons](https://commons.wikimedia.org/).
2. Log in to your account.
3. Go to **Preferences > Gadgets > Interface: Files and categories**.
4. Select the **Global Usage Badges**.
5. Select **Save**.

To use the gadget:
1. Visit any page in Commons with images, for example [Category:*Geodorcus helmsi*](https://commons.wikimedia.org/wiki/Category:Geodorcus_helmsi).
2. Click the red square with the question mark in the lower left hand corner of any image.
3. A number is now displayed showing how many times the image is used across Wikipedia.
4. Hover over the number for a quick preview of where the image is used.

For more information, see the [Global Usage Badges gadget help page](https://commons.wikimedia.org/wiki/Help:Gadget-GlobalUsageUI).

Source: [Wikipedia Weekly Network - LIVE Wikidata editing #105](https://www.youtube.com/watch?v=lSDctjhemNc)
